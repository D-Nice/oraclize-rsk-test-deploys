### STEPS


1 - `npm install -g truffle@4.1.3 && npm i`

2 - `truffle console --network rsk-testnet`

3 - `migrate`

NOTE if any contracts fail during deployment, comment them out from 2_deploy_contracts.js. Random and YoutubeViews_2 are commented out, as they fail to deploy, and deploy failures won't save artifacts of ones that succeeded for testing. To test failures, uncomment them from the aforementioned deploy script. 

4 - `YoutubeViews_1.deployed().then(x => x.update())`

5 - `YoutubeViews_1.deployed().then(x => x.getOAR())`

6 - `YoutubeViews_1.deployed().then(x => x.viewsCount())`


