const HDWalletProvider = require("truffle-hdwallet-provider")
const mnemonic = "orange apple banana"
module.exports = {
  networks: {
    development: {
      host: "127.0.0.1",
      port: 8545,
      network_id: "*"
    },
    'rsk-testnet': {
      provider: () => new HDWalletProvider(mnemonic, "https://public-node.testnet.rsk.co")
      , network_id: 44444
      , gas: 6000000
      , gasPrice: 100000
    }
  }
}
