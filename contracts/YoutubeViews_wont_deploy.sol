pragma solidity ^0.4.0;

import "./oraclize/usingOraclize.sol";

contract YoutubeViews_2 is usingOraclize {

  string public viewsCount;


  // won't deploy trying to run update from within constructor or even setting proof type (involvest external call)
  function YoutubeViews_2() {
    update();
  }

  function __callback(bytes32 myid, string result) {
    viewsCount = result;
    // ...Do something with viewsCount, like maybe tipping the author if viewsCount > X...?
  }

  function update() payable {
    oraclize_query('URL', 'html(https://www.youtube.com/watch?v=9bZkp7q19f0).xpath(//*[contains(@class, "watch-view-count")]/text())');
  }

  function getOAR() view returns (address) {
    return OAR;
  }

  function getCON() view returns (address) {
    return OAR.getAddress();
  }

  function price() view returns (uint){
    return oraclize.getPrice("URL");
  }
}
