pragma solidity ^0.4.0;

import "./oraclize/usingOraclize.sol";

contract YoutubeViews_1 is usingOraclize {

  string public viewsCount;


  // works with empty constructor (or at least one that doesn't do external calls upon deployment)
  function YoutubeViews_1() {
  }

  function __callback(bytes32 myid, string result) {
    viewsCount = result;
    // ...Do something with viewsCount, like maybe tipping the author if viewsCount > X...?
  }

  function update() payable {
    oraclize_query('URL', 'html(https://www.youtube.com/watch?v=9bZkp7q19f0).xpath(//*[contains(@class, "watch-view-count")]/text())');
  }

  function getOAR() view returns (address) {
    return OAR;
  }

  function getCON() view returns (address) {
    return OAR.getAddress();
  }

  function price() view returns (uint){
    return oraclize.getPrice("URL");
  }
}
