const contracts = [
  artifacts.require('./YoutubeViews_1.sol'),
  artifacts.require('./YoutubeViews_2.sol'),
  // uncomment below to see deploy fail (note it won't save artifacts)
  // artifacts.require('./RandomExample.sol'),
]

module.exports = deployer => 
  contracts.map(contract => 
      deployer.deploy(contract))
